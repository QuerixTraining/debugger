GLOBALS "globals.4gl"

MAIN
	DEFINE myDynamicRecordArray DYNAMIC ARRAY OF 
		RECORD 
			f1,f2,f3 STRING 
		END RECORD
	DEFINE i INT
	
	#initialise a dynamic array
	FOR i = 1 TO 5 
		LET myDynamicRecordArray[i].f1 = "Field f1 row:", trim(i)
		LET myDynamicRecordArray[i].f2 = "Field f2 row:", trim(i)
		LET myDynamicRecordArray[i].f3 = "Field f3 row:", trim(i)
	END FOR  

	CALL manipulateGlobalVariables()
	
	
	CALL fgl_winmessage("End of Demo","End of Debugger Demo Run","info")

	DISPLAY ""
	DISPLAY "*********************************************************"
	DISPLAY "End of Debugger Demonstration"
	DISPLAY "*********************************************************"
	
END MAIN

##########################################################
# FUNCTION manipulateGlobalVariables()
##########################################################
FUNCTION manipulateGlobalVariables()
	DEFINE i, joker INT
	
	#We demonstrate the 2 conditional breakPoint properties
	# Breakpoint on n hit (suspend debugger if this line was processed the "n"-th time
	# Breakpoint if a specified condition is true i.e. X=50

	DISPLAY "*****************************************************"
	DISPLAY "5. Conditional BreakPoints"	
	DISPLAY "We are now going to show the conditional break point properties"
	DISPLAY "HIT count and variable based conditions"
	DISPLAY "These conditional breakpoint examples will be demonstrated in a FOR LOOP"
	DISPLAY "*****************************************************"
	DISPLAY ""
	

	FOR i=1 TO 200  

		IF i = 50 THEN --hit a break point on the 50th hit/encounter 
			DISPLAY "*BreakPoint"
			DISPLAY "->BreakPoint property HIT<-"
			DISPLAY "FOR i=1 TO 200  #Has got a a HIT=50 conditional break point"
			DISPLAY "After this line is process for the 50th time, the debugger will suspend"
			DISPLAY ""
				
		END IF
					#IF joker = 50 THEN
					#	DISPLAY "*BreakPoint"					
					#	DISPLAY "\"LET glInt1 = i\"  #Has got the condition JOKER=95"
					#	DISPLAY "Variable JOKER=", trim(joker)
					#	DISPLAY "When the debugger processes this line AND the variable joker = 50, the debugger will suspend"
					#	DISPLAY ""
					#END IF
		IF joker = 95 THEN
			DISPLAY "*BreakPoint"					
			DISPLAY "\"LET glInt1 = i\"  #Has got the condition JOKER=95"
			DISPLAY "Variable JOKER=", trim(joker)
			DISPLAY "When the debugger processes this line AND the variable joker = 95, the debugger will suspend"
			DISPLAY ""
		END IF

		IF joker = 100 THEN
						DISPLAY "*BreakPoint"		
						DISPLAY "\"LET joker = i+77\"  #Has got the condition JOKER=100"
						DISPLAY "Variable JOKER=", trim(joker)
						DISPLAY "After this line is process AND the variable joker=100, the debugger will suspend"
						DISPLAY ""
		END IF

		LET glInt1 = i		--this break point is conditional -> joker = 95
		LET joker = i+77  --this break point is conditional -> joker = 100
	END FOR
	
	LET glString1 =  "* ", glString1, " Changed by manipulateGlobalVariables()"
	LET glDate1 = 	glDate1 + 365  
	
END FUNCTION