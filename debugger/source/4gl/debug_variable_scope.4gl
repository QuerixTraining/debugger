GLOBALS
	DEFINE glGlobalInt INT
END GLOBALS

#Module scope variable
DEFINE mdModuleDate DATE


MAIN
	DEFINE lcLocalString STRING -- local scope

	OPTIONS
		AUTOREFRESH 3		-- For screen update 
										-- 1=only on CALL fgl_ui_refresh() 
										-- 2=on input/prompt 
										-- 3=send everything immediately to the screen
	DEFER INTERRUPT

	OPEN WINDOW myWin WITH FORM "form/debug_variable_scope"	
	LET lcLocalString = "My String"
	LET mdModuleDate = "12/12/2016"
	LET glGlobalInt = 999

	DISPLAY BY NAME int_flag  
	
	
	WHILE TRUE
		
		INPUT BY NAME 
			glGlobalInt, mdModuleDate, lcLocalString
			WITHOUT DEFAULTS
			
		IF int_flag = TRUE THEN  #special build in variable
			DISPLAY BY NAME int_flag  

			DISPLAY "User pressed Cancel"
			DISPLAY "See you later alligator"

			LET int_flag = FALSE
			DISPLAY BY NAME int_flag  
			
			EXIT WHILE
		END IF
	
	END WHILE
	
END MAIN