GLOBALS "globals.4gl"

##########################################################
# FUNCTION initData3(lcFuncString,lcFuncInt,lcFuncDate)
##########################################################
FUNCTION initData3(lcFuncString,lcFuncInt,lcFuncDate)
	DEFINE lcFuncString STRING
	DEFINE lcFuncInt INT
	DEFINE lcFuncDate DATE
	DISPLAY "*BreakPoint"
	DISPLAY "Now, we are in a program function in a different 4gl file"
	DISPLAY "which was again called with the 3 arguments"
	DISPLAY "lcFuncString, lcFuncInt, lcFuncDate"
	DISPLAY ""
	
	LET lcFuncString = lcFuncString, "???"
	LET lcFuncInt = lcFuncInt + 2000
	LET lcFuncDate = lcFuncDate + 730

	DISPLAY "*BreakPoint"
	DISPLAY "Now, these 3 passed arguments were changed and will be returned"
	DISPLAY "RETURNING lcFuncString, lcFuncInt, lcFuncDate"
	DISPLAY ""
		
	RETURN  lcFuncString,lcFuncInt,lcFuncDate
	
END FUNCTION	