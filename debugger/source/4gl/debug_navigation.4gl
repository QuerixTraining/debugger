GLOBALS
	DEFINE glGlobalInt INT
END GLOBALS

#Module scope variable
DEFINE mdModuleDate DATE


MAIN
	DEFINE lcLocalString STRING -- local scope
	DEFINE x INT

	OPTIONS
		AUTOREFRESH 3		-- For screen update 
										-- 1=only on CALL fgl_ui_refresh() 
										-- 2=on input/prompt 
										-- 3=send everything immediately to the screen
	DEFER INTERRUPT

	OPEN WINDOW myWin WITH FORM "form/debug_variable_scope"	
	LET lcLocalString = "My String"
	LET mdModuleDate = "12/12/2016"
	LET glGlobalInt = 999

	DISPLAY BY NAME int_flag  

	CALL fgl_winmessage("Hello World","Hello World","info")
		
	CALL myFunc1()
	CALL myFunc2()
	CALL myFunc3()
	CALL myFuncLongLoop()
	
	WHILE TRUE
		
		INPUT BY NAME 
			glGlobalInt, mdModuleDate, lcLocalString
			WITHOUT DEFAULTS
			
			AFTER FIELD lcLocalString
				LET x = 1

			AFTER FIELD mdModuleDate
				LET x = 1

			AFTER FIELD glGlobalInt
				LET x = 1
			
		END INPUT 
		
		IF int_flag = TRUE THEN  #special build in variable
			DISPLAY BY NAME int_flag  

			DISPLAY "User pressed Cancel"
			DISPLAY "See you later alligator"

			LET int_flag = FALSE
			DISPLAY BY NAME int_flag  
			
			EXIT WHILE
		END IF
	
	END WHILE

	
END MAIN


FUNCTION myFuncLongLoop()
	DEFINE i int
	DEFINE myBoolean BOOLEAN

	LET i = 0
	LET myBoolean = TRUE
	
	WHILE myBoolean = TRUE 
		LET i = i + 1
	END WHILE
	
END FUNCTION	


FUNCTION myFunc1()
	DEFINE x int
		
	DISPLAY "In Function myFunc1"
	
	LET x = 1
	LET x = 2
	LET x = 3
	LET x = 4
	LET x = 5
END FUNCTION

FUNCTION myFunc2()
	DEFINE x int
		
	DISPLAY "In Function myFunc2"
	
	LET x = 1
	LET x = 2
	LET x = 3
	LET x = 4
	LET x = 5
END FUNCTION

FUNCTION myFunc3()
	DEFINE x int
		
	DISPLAY "In Function myFunc3"
	
	LET x = 1
	LET x = 2
	LET x = 3
	LET x = 4
	LET x = 5
END FUNCTION


