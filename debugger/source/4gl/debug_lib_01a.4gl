GLOBALS "globals.4gl"

##########################################################
# FUNCTION initDataInLib(lcFuncString,lcFuncInt,lcFuncDate)
##########################################################
FUNCTION initDataInLib(lcFuncString,lcFuncInt,lcFuncDate)
	DEFINE lcFuncString STRING
	DEFINE lcFuncInt INT
	DEFINE lcFuncDate DATE

	DISPLAY "*BreakPoint"
	DISPLAY "Now, we are in a library function which was again called with the 3 arguments"
	DISPLAY "lcFuncString, lcFuncInt, lcFuncDate"
	DISPLAY ""

	LET lcFuncString = lcFuncString, "-lib-###"
	LET lcFuncInt = lcFuncInt + 3000
	LET lcFuncDate = lcFuncDate + 1095

	DISPLAY "*BreakPoint"
	DISPLAY "Now, these 3 passed arguments were changed and will be returned"
	DISPLAY "RETURNING lcFuncString, lcFuncInt, lcFuncDate"
	DISPLAY ""

		
	RETURN  lcFuncString,lcFuncInt,lcFuncDate
	
END FUNCTION


##########################################################
# FUNCTION demontrateGlobalConditionalSuspend()
##########################################################
FUNCTION demontrateExpressions()
	DEFINE i INT

	DISPLAY "*****************************************************"
	DISPLAY "6. Expressions"	
	DISPLAY "We are now going to show expressions"
	DISPLAY "NOTE: Expressions are shown in red/invalid, if the used variable(s) are not in the current program scope"
	DISPLAY "These expressions will be shown again in a a FOR LOOP"
	DISPLAY "This FOR LOOP manipulates the local variable i AND"
	DISPLAY "the global scope variable glInt2"	
	DISPLAY "*****************************************************"
	DISPLAY ""
	
	DISPLAY "*BreakPoint"
	DISPLAY "This FOR LOOP manipulates the global scope variable glInt2"
	DISPLAY "by setting it to the FOR loop control value"	
	DISPLAY "Note: Open the Expressions View/Panel"
	DISPLAY ""
			
	FOR i = 0 TO 1000
		LET glInt2 = i
		
		CASE i
			WHEN 400
				DISPLAY "*Check the expression result"
				DISPLAY "i / glInt2 >400"
				DISPLAY "glInt2 = ", trim(glInt2), " i=", trim(i)
				DISPLAY ""

			WHEN 666
				DISPLAY "*Check the expression result"
				DISPLAY "i / glInt2 =666"				
				DISPLAY "glInt2 = ", trim(glInt2), " i=", trim(i)
				DISPLAY ""
				
		END CASE
		
	END FOR
	

END FUNCTION	