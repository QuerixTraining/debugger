#Example Program to demonstrate the debugger facilities
GLOBALS "globals.4gl"

DEFINE
	mdString1 STRING,
	mdInt1 INTEGER, 
	mdDate1 DATE


##########################################################
# MAIN
##########################################################
MAIN
	DEFINE lcString1,lcString2,lcString3,lcString4 STRING
	DEFINE lcInt1,lcInt2,lcInt3,lcInt4 INTEGER
	DEFINE lcDate1,lcDate2,lcDate3,lcDate4 DATE
	DEFINE myDynamicRecordArray DYNAMIC ARRAY OF 
		RECORD 
			f1,f2,f3 STRING 
		END RECORD
	DEFINE i SMALLINT --for loop control variable
	#initialize the local dynamic array to see how it is represented in the variable panel
	-- we only assign data for row 1-5

	DISPLAY "*****************************************************"
	DISPLAY "1. Variable Scopes, Filters, Complex Data Types and variable manipulations"	
	DISPLAY "In the first part, we are going to show the different variable scopes"
	DISPLAY "And how to apply the corresponding variable-scope view-filters"
	DISPLAY "Also notice, that changed variables are shown with a yellow background"
	DISPLAY "*****************************************************"
	DISPLAY ""

					
	#initialize the global variables
	LET glString1 = "This is a global scope variable string (initialised)"
	LET glInt1 = 555
	LET glDate1 = "10/10/1865"

	DISPLAY "*BreakPoint"
	DISPLAY "->Global Scope Variable View Filter<-"
	DISPLAY "Look at how global variables (glString1, glInt1, glDate1)"
	DISPLAY "are shown in the variable panel"	
	DISPLAY ""
		
	#initialize the module variables
	LET mdString1 = "This is a module scope variable string (initialised)"
	LET mdInt1 = 55555
	LET mdDate1 = "10/10/1955"

	DISPLAY "*BreakPoint"
	DISPLAY "->Module Scope Variable View Filter<-"	
	DISPLAY "Look at how the module variables (mdString1, mdInt1, mdDate1)"
	DISPLAY "are shown in the variable panel"
	DISPLAY ""
	
	#initialize local scope variables
	LET lcString1 = "My First local String"
	LET lcInt1 = 123
	LET lcDate1 = "12/12/2016"

	DISPLAY "*BreakPoint"
	DISPLAY "->Local Variable View Filter<-"	
	DISPLAY "Look at how the local variables (lcString1, lcInt1, lcDate1)"
	DISPLAY "are shown in the variable panel"	
	DISPLAY ""

	FOR i = 1 TO 5 
		LET myDynamicRecordArray[i].f1 = "Field f1 row:", trim(i)
		LET myDynamicRecordArray[i].f2 = "Field f2 row:", trim(i)
		LET myDynamicRecordArray[i].f3 = "Field f3 row:", trim(i)
	END FOR  

	DISPLAY "*BreakPoint"
	DISPLAY "->variables Panel: Local Variable Records and Arrays<-"	
	DISPLAY "Look at how this local scope dynamic array record"
	DISPLAY "is shown in the variable panel"	
	DISPLAY "<myDynamicRecordArray> has got ", trim(myDynamicRecordArray.getsize()), " elements/rows!" 
	DISPLAY ""



	DISPLAY "*****************************************************"
	DISPLAY "2. Debugging written functions within the program"	
	#DISPLAY "In the first part, we are going to show the different variable scopes"
	#DISPLAY "And how to apply the corresponding variable-scope view-filters"
	#DISPLAY "Also notice, that changed variables are shown with a yellow background"
	DISPLAY "*****************************************************"
	DISPLAY ""

	#Calling the program FUNCTION initData2() in the same 4GL file
	CALL initData2("My Second local String",456,"12/12/2018")
		RETURNING lcString2, lcInt2, lcDate2

	DISPLAY "*****************************************************"
	DISPLAY "3. Debugging written functions in other 4GL files"
	DISPLAY " (within the program)"	
	#DISPLAY "In the first part, we are going to show the different variable scopes"
	#DISPLAY "And how to apply the corresponding variable-scope view-filters"
	#DISPLAY "Also notice, that changed variables are shown with a yellow background"
	DISPLAY "*****************************************************"
	DISPLAY ""
	
	#Calling the program FUNCTION initData3() in another 4GL file
	CALL initData3("My Third local String",789,"12/12/2020")
		RETURNING lcString3, lcInt3, lcDate3	


	DISPLAY "*****************************************************"
	DISPLAY "4. Debugging written functions located in Libraries"
	#DISPLAY "In the first part, we are going to show the different variable scopes"
	#DISPLAY "And how to apply the corresponding variable-scope view-filters"
	#DISPLAY "Also notice, that changed variables are shown with a yellow background"
	DISPLAY "*****************************************************"
	DISPLAY ""

	#Calling the library FUNCTION initDataInLib() located in the library debug_lib_01
	CALL initDataInLib("My Fourth (Lib) local String",1123,"12/12/2022")
		RETURNING lcString4, lcInt4, lcDate4	

	#Manipulate some global variables in a function
	CALL manipulateGlobalVariables()	--this shows conditional break points and expressions

	#Next function call is used to show expressions
	CALL demontrateExpressions()  --this shows expressions and user-forced Suspend


	#Suspend the process manually by clicking the SUSPEND button in the toolbar
	DISPLAY "*****************************************************"
	DISPLAY "7. Suspend the program flow / debugger manually"
	#DISPLAY "In the first part, we are going to show the different variable scopes"
	#DISPLAY "And how to apply the corresponding variable-scope view-filters"
	#DISPLAY "Also notice, that changed variables are shown with a yellow background"
	DISPLAY "*****************************************************"
	DISPLAY ""
	CALL loopToShowSuspend(200000)
	
	
	DISPLAY "########################################################"	
	DISPLAY "Show the variable values before the program terminates"	
	DISPLAY "########################################################"
	DISPLAY "glString1", glString1
	DISPLAY "glInt1", glInt1
	DISPLAY "glDate1", glDate1

	DISPLAY "########################################################"
	DISPLAY "mdString1=", mdString1
	DISPLAY "mdlInt1=", mdInt1
	DISPLAY "mdDate1", mdDate1

	DISPLAY "########################################################"	
	DISPLAY "lcString1=",lcString1
	DISPLAY "lcString2=", lcString2
	DISPLAY "lcString3=",lcString3
	DISPLAY "lcString4=", lcString4

	DISPLAY "########################################################"	
	DISPLAY "lcInt1=", lcInt1
	DISPLAY "lcInt2=", lcInt2
	DISPLAY "lcInt3=", lcInt3 
	DISPLAY "lcInt4=", lcInt4

	DISPLAY "########################################################"
	DISPLAY "lcDate1=", lcDate1
	DISPLAY "lcDate2=", lcDate2
	DISPLAY "lcDate3=", lcDate3
	DISPLAY "lcDate4=", lcDate4

	CALL fgl_winmessage("End of Demo","End of Debugger Demo Run","info")
	DISPLAY ""
	DISPLAY "*********************************************************"
	DISPLAY "End of Debugger Demonstration"
	DISPLAY "*********************************************************"

END MAIN

##########################################################
# FUNCTION initData2(lcFuncString,lcFuncInt,lcFuncDate)
##########################################################
FUNCTION initData2(lcFuncString,lcFuncInt,lcFuncDate)
	DEFINE lcFuncString STRING
	DEFINE lcFuncInt INT
	DEFINE lcFuncDate DATE

	DISPLAY "*BreakPoint"
	DISPLAY "Now, we are in a program function FUNCTION initData2()"
	DISPLAY "which was called with the 3 arguments"
	DISPLAY "lcFuncString, lcFuncInt, lcFuncDate"
	DISPLAY ""

	LET lcFuncString = lcFuncString, "***"
	LET lcFuncInt = lcFuncInt + 1000
	LET lcFuncDate = lcFuncDate + 365

	DISPLAY "*BreakPoint"
	DISPLAY "Now, these 3 passed arguments were changed and will be returned"
	DISPLAY "RETURNING lcFuncString, lcFuncInt, lcFuncDate"
	DISPLAY ""
		
	RETURN  lcFuncString,lcFuncInt,lcFuncDate
	
END FUNCTION	


FUNCTION loopToShowSuspend(argLoopLength)
	DEFINE i, argLoopLength INT

	DISPLAY ""
	DISPLAY "*Trying user invoked Suspend"
	DISPLAY "Press the Debugger Suspend button in the Toolbar Now!"
	DISPLAY ""
	
	FOR i = 1 TO argLoopLength
	END FOR
	
END FUNCTION	